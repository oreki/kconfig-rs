//! Contains constructs defined in[`KEMailSettings`][header] header
//!
//! [header]: https://api.kde.org/frameworks/kconfig/html/classKEMailSettings.html

use cpp::{cpp, cpp_class};
use qttypes::{QString, QStringList};

cpp! {{
    #include <KEMailSettings>
}}

#[repr(C)]
pub enum Setting {
    ClientProgram,
    ClientTerminal,
    RealName,
    EmailAddress,
    ReplyToAddress,
    Organization,
    OutServer,
    OutServerLogin,
    OutServerPass,
}

cpp_class!(pub unsafe struct KEMailSettings as "KEMailSettings");

impl KEMailSettings {
    /// Returns the name of the default profile.
    pub fn default_profile_name(&self) -> QString {
        cpp!(unsafe [self as "const KEMailSettings*"] -> QString as "QString" {
            return self->defaultProfileName();
        })
    }

    /// Get one of the predefined "basic" settings.
    /// # Parameters
    /// * `s` = the setting to get
    pub fn get_setting(&self, s: Setting) -> QString {
        cpp!(unsafe [self as "const KEMailSettings*", s as "KEMailSettings::Setting"] -> QString as "QString" {
            return self->getSetting(s);
        })
    }

    /// List of profiles available.
    pub fn profiles(&self) -> QStringList {
        cpp!(unsafe [self as "const KEMailSettings*"] -> QStringList as "QStringList" {
            return self->profiles();
        })
    }

    /// Sets a new default.
    pub fn set_default(&mut self, def: QString) {
        cpp!(unsafe [self as "KEMailSettings*", def as "QString"] {
            self->setDefault(def);
        })
    }

    /// Change the current profile.
    /// # Parameters
    /// * `v` = 	the name of the new profile
    pub fn set_profile(&mut self, v: QString) {
        cpp!(unsafe [self as "KEMailSettings*", v as "QString"] {
            self->setProfile(v);
        })
    }

    /// Set one of the predefined "basic" settings.
    /// # Parameters
    /// * `s` = the setting to set
    /// * `v` = 	the new value of the setting, or `QString::default()` to unset
    pub fn set_setting(&mut self, s: Setting, v: QString) {
        cpp!(unsafe [self as "KEMailSettings*",s as "KEMailSettings::Setting", v as "QString"] {
            self->setSetting(s, v);
        })
    }
}
