use std::{
    fmt::Display,
    sync::{MutexGuard, PoisonError},
};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, PartialEq)]
pub enum Error {
    MutexError,
    CStringError,
    EntryNotFound(String),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MutexError => write!(f, "Error in locking mutex"),
            Self::CStringError => write!(f, "Error in constructing CString"),
            Self::EntryNotFound(x) => write!(f, "Entry for Key {} not found", x),
        }
    }
}

impl<T> From<PoisonError<MutexGuard<'_, T>>> for Error {
    fn from(_: PoisonError<MutexGuard<'_, T>>) -> Self {
        Self::MutexError
    }
}

impl From<std::ffi::NulError> for Error {
    fn from(_: std::ffi::NulError) -> Self {
        Self::CStringError
    }
}
