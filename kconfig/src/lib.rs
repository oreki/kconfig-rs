//! # Introduction
//! KConfig is persistent platform-independent application settings framework built by KDE. This
//! crate is a wrapper for the C++ framework and allows using it from Rust.
//! It can be used along with crates like [qmetaobject-rs](https://github.com/woboq/qmetaobject-rs)
//! to create QT applications entirely in Rust.
//!
//! # Helpful Links about using KConfig
//! - [KDE Frameworks – Part 1](https://www.kdab.com/kde-frameworks-part-1-kconfig/)
//! - [KConfig C++ API](https://api.kde.org/frameworks/kconfig/html/index.html)

pub mod errors;
pub(crate) mod helpers;
pub mod kconfig;
pub mod kconfigbase;
pub mod kconfiggroup;
#[cfg(feature = "kconfigxt")]
pub mod kcoreconfigskeleton;
pub mod kdesktopfile;
pub mod kemailsettings;
pub mod ksharedconfig;

/// Module intended for Global Import
pub mod prelude {
    pub use crate::errors::Error;
    pub use crate::kconfig::{KConfig, KConfigTrait, OpenFlags};
    pub use crate::kconfigbase::{KConfigBase, WriteConfigFlags};
    pub use crate::kconfiggroup::KConfigGroup;
    pub use crate::ksharedconfig::{KSharedConfig, KSharedConfigPtr};
}
