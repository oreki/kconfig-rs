//! Contains constructs defined in[`KDesktopFile`][header] header
//!
//! [header]: https://api.kde.org/frameworks/kconfig/html/classKDesktopFile.html

use cpp::{cpp, cpp_class};
use qttypes::{QByteArray, QStandardPathLocation, QString, QStringList};

use crate::{
    helpers::qstring_option,
    kconfig::{KConfigImpl, KConfigTrait, OpenFlags},
    kconfigbase::{AccessMode, KConfigBase, KConfigBaseImpl, WriteConfigFlags},
    kconfiggroup::KConfigGroup,
};

cpp! {{
    #include <KDesktopFile>

    struct KDesktopFileHolder {
        std::unique_ptr<KDesktopFile> d;

        KDesktopFileHolder(QStandardPaths::StandardLocation resourceType, const QString &fileName): d(new KDesktopFile(resourceType, fileName)) {}
        KDesktopFileHolder(const QString &fileName): d(new KDesktopFile(fileName)) {}
    };

}}

cpp_class!(
/// Struct representing [`KDesktopFile`][class] class.
///
/// [class]: https://api.kde.org/frameworks/kconfig/html/classKDesktopFile.html
pub unsafe struct KDesktopFile as "KDesktopFileHolder"
);

impl KDesktopFile {
    pub fn new(resource_type: QStandardPathLocation, file_name: QString) -> Self {
        cpp!(unsafe [resource_type as "QStandardPaths::StandardLocation", file_name as "QString"] -> KDesktopFile as "KDesktopFileHolder" {
            return KDesktopFileHolder(resource_type, file_name);
        })
    }

    pub fn from_file(file_name: QString) -> Self {
        cpp!(unsafe [file_name as "QString"] -> KDesktopFile as "KDesktopFileHolder" {
            return KDesktopFileHolder(file_name);
        })
    }

    /// Checks whether the user is authorized to run this desktop file.
    pub fn is_authorized_desktop_file(path: QString) -> bool {
        cpp!(unsafe [path as "QString"] -> bool as "bool" {
            return KDesktopFile::isAuthorizedDesktopFile(path);
        })
    }

    /// Checks whether this is really a desktop file.
    /// The check is performed looking at the file extension (the file is not opened).
    /// Currently, the only valid extension is ".desktop".
    pub fn is_desktop_file(path: QString) -> bool {
        cpp!(unsafe [path as "QString"] -> bool as "bool" {
            return KDesktopFile::isDesktopFile(path);
        })
    }

    /// Returns the location where changes for the .desktop file path should be written to.
    pub fn locate_local(path: QString) -> QString {
        cpp!(unsafe [path as "QString"] -> QString as "QString" {
            return KDesktopFile::locateLocal(path);
        })
    }

    /// Returns the name of the .desktop file that was used to construct this KDesktopFile.
    pub fn file_name(&self) -> QString {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->fileName();
        })
    }

    /// Returns the value of the "GenericName=" entry.
    pub fn read_generic_name(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readGenericName();
        });
        qstring_option(t)
    }

    /// Returns the value of the "Name=" entry.
    pub fn read_name(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readName();
        });
        qstring_option(t)
    }

    /// Returns the value of the "Path=" entry.
    pub fn read_path(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readPath();
        });
        qstring_option(t)
    }

    /// Returns the value of the "Type=" entry.
    pub fn read_type(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readType();
        });
        qstring_option(t)
    }

    /// Returns the value of the "Url=" entry.
    pub fn read_url(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readUrl();
        });
        qstring_option(t)
    }

    /// Returns the value of the "Comment=" entry.
    pub fn read_comment(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readComment();
        });
        qstring_option(t)
    }

    /// Returns the value of the "X-DocPath=" Or "DocPath=" entry.
    pub fn read_doc_path(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readDocPath();
        });
        qstring_option(t)
    }

    /// Returns the value of the "Icon=" entry.
    pub fn read_icon(&self) -> Option<QString> {
        let t = cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->readIcon();
        });
        qstring_option(t)
    }

    /// Returns true if the action group exists, false otherwise.
    pub fn has_action_group(&self, group: QString) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*", group as "QString"] -> bool as "bool" {
            return self->d->hasActionGroup(group);
        })
    }

    /// Checks whether there is an entry "Type=Application".
    pub fn has_application_type(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->hasApplicationType();
        })
    }

    /// Checks whether there is an entry "Type=FSDevice".
    pub fn has_device_type(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->hasDeviceType();
        })
    }

    /// Checks whether there is an entry "Type=Link".
    pub fn has_link_type(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->hasLinkType();
        })
    }

    /// Whether the entry should be suppressed in menus.
    /// This handles the NoDisplay key, but also OnlyShowIn / NotShowIn.
    /// Returns true to suppress this service
    pub fn no_display(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->noDisplay();
        })
    }

    /// Checks whether the TryExec field contains a binary which is found on the local system.
    pub fn try_exec(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->tryExec();
        })
    }

    /// Returns a list of the "Actions=" entries.
    pub fn read_actions(&self) -> QStringList {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QStringList as "QStringList" {
            return self->d->readActions();
        })
    }

    /// Returns a list of the "MimeType=" entries.
    pub fn read_mime_types(&self) -> QStringList {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QStringList as "QStringList" {
            return self->d->readMimeTypes();
        })
    }

    /// Sets the desktop action group.
    pub fn action_group(&self, group: QString) -> KConfigGroup {
        cpp!(unsafe [self as "const KDesktopFileHolder*", group as "QString"] -> KConfigGroup as "KConfigGroup" {
            return self->d->actionGroup(group);
        })
    }

    pub fn desktop_group(&self) -> KConfigGroup {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> KConfigGroup as "KConfigGroup" {
            return self->d->desktopGroup();
        })
    }
}

impl KConfigBase for KDesktopFile {
    fn access_mode(&self) -> AccessMode {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> AccessMode as "KConfigBase::AccessMode" {
            return self->d->accessMode();
        })
    }

    fn mark_as_clean(&mut self) {
        cpp!(unsafe [self as "KDesktopFileHolder*"] {
            self->d->markAsClean();
        });
    }

    fn sync(&mut self) -> bool {
        cpp!(unsafe [self as "KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->sync();
        })
    }

    fn is_immutable(&self) -> bool {
        cpp!(unsafe [self as "const KConfigHolder*"] -> bool as "bool" {
            return self->d->isImmutable();
        })
    }
}

impl KConfigTrait for KDesktopFile {
    fn is_dirty(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->isDirty();
        })
    }

    fn is_config_writable(&self, warn_user: bool) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*", warn_user as "bool"] -> bool as "bool" {
            return self->d->isConfigWritable(warn_user);
        })
    }

    fn open_flags(&self) -> OpenFlags {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> OpenFlags as "KConfig::OpenFlags" {
            return self->d->openFlags();
        })
    }

    fn reparse_configuration(&mut self) {
        cpp!(unsafe [self as "KDesktopFileHolder*"] {
            self->d->reparseConfiguration();
        });
    }

    fn set_read_defaults(&mut self, b: bool) {
        cpp!(unsafe [self as "KDesktopFileHolder*", b as "bool"] {
            self->d->setReadDefaults(b);
        });
    }

    fn read_defaults(&self) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> bool as "bool" {
            return self->d->readDefaults();
        })
    }

    fn location_type(&self) -> QStandardPathLocation {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QStandardPathLocation as "QStandardPaths::StandardLocation" {
            return self->d->locationType();
        })
    }
}

impl KConfigBaseImpl for KDesktopFile {
    unsafe fn delete_group_impl(&mut self, group: QByteArray, flags: WriteConfigFlags) {
        cpp!(unsafe [self as "KDesktopFileHolder*", group as "QByteArray", flags as "KConfigBase::WriteConfigFlags"] {
            self->d->deleteGroup(group, flags);
        });
    }

    fn group_impl(&mut self, group: QByteArray) -> KConfigGroup {
        cpp!(unsafe [self as "KDesktopFileHolder*", group as "QByteArray"] -> KConfigGroup as "KConfigGroup" {
            return self->d->group(group);
        })
    }

    fn has_group_impl(&self, group: QByteArray) -> bool {
        cpp!(unsafe [self as "const KDesktopFileHolder*", group as "QByteArray"] -> bool as "bool" {
            return self->d->hasGroup(group);
        })
    }

    fn group_list_impl(&self) -> QStringList {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QStringList as "QStringList" {
            return self->d->groupList();
        })
    }
}

impl KConfigImpl for KDesktopFile {
    fn add_config_sources_impl(&mut self, sources: QStringList) {
        cpp!(unsafe [self as "KDesktopFileHolder*", sources as "QStringList"] {
            self->d->addConfigSources(sources);
        });
    }

    fn additional_config_sources_impl(&self) -> QStringList {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QStringList as "QStringList" {
            return self->d->additionalConfigSources();
        })
    }

    fn check_update_impl(&mut self, id: QString, update_file: QString) {
        cpp!(unsafe [self as "KDesktopFileHolder*", id as "QString", update_file as "QString"] {
            self->d->checkUpdate(id, update_file);
        });
    }

    fn set_locale_impl(&mut self, locale: QString) -> bool {
        cpp!(unsafe [self as "KDesktopFileHolder*", locale as "QString"] -> bool as "bool" {
            return self->d->setLocale(locale);
        })
    }

    fn locale_impl(&self) -> QString {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->locale();
        })
    }

    fn name_impl(&self) -> QString {
        cpp!(unsafe [self as "const KDesktopFileHolder*"] -> QString as "QString" {
            return self->d->name();
        })
    }
}
