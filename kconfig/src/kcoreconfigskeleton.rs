use qttypes::{QByteArray, QStandardPathLocation, QString, QVariant};

use crate::errors;
use crate::kconfig::KConfig;
use crate::kconfigbase::{KConfigBase, WriteConfigFlags};
use crate::kconfiggroup::KConfigGroup;
use crate::prelude::OpenFlags;

#[derive(Default, Debug)]
pub struct KCoreConfigSkeleton {
    config: KConfig,
    current_group: Option<KConfigGroup>,
}

impl KCoreConfigSkeleton {
    pub fn new<T: Into<QString>>(config_name: T) -> Self {
        Self {
            config: KConfig::new(
                config_name,
                OpenFlags::FULL_CONFIG,
                QStandardPathLocation::GenericConfigLocation,
            ),
            current_group: None,
        }
    }

    pub fn set_current_group<T: Into<QByteArray>>(&mut self, group: T) {
        self.current_group = Some(self.config.group(group));
    }

    pub fn add_item<K, V>(&mut self, key: K, val: V) -> errors::Result<()>
    where
        K: Into<Vec<u8>>,
        V: Into<QVariant>,
    {
        match &mut self.current_group {
            Some(group) => group.write_qvariant_entry(key, val.into(), WriteConfigFlags::NORMAL),
            None => panic!("No Current Group"),
        }
    }
}
