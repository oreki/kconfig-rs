use std::ffi::CStr;

use crate::errors;
use qttypes::{QString, QStringList};

#[inline]
pub fn qstring_option(qstr: QString) -> Option<QString> {
    if qstr.is_null() {
        None
    } else {
        Some(qstr)
    }
}

#[inline]
pub fn qstring_entry_check(key: &CStr, qstr: QString) -> errors::Result<QString> {
    if qstr.is_null() {
        Err(errors::Error::EntryNotFound(
            key.to_string_lossy().to_string(),
        ))
    } else {
        Ok(qstr)
    }
}

#[inline]
pub fn qstringlist_entry_check(key: &CStr, qstr: QStringList) -> errors::Result<QStringList> {
    if qstr.len() == 0 {
        Err(errors::Error::EntryNotFound(
            key.to_string_lossy().to_string(),
        ))
    } else {
        Ok(qstr)
    }
}
