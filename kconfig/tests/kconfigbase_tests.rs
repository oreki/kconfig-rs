use kconfig::kconfig::{KConfig, OpenFlags};
use kconfig::kconfigbase::{AccessMode, KConfigBase, WriteConfigFlags};
use qttypes::{QStandardPathLocation, QString};

#[test]
fn write_config_flags() {
    assert_eq!(WriteConfigFlags::NORMAL, WriteConfigFlags::PERSISTENT);
}

#[test]
fn kconfigclass() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general = config.group("General");

    general
        .write_qvariant_entry("someBool", true.into(), WriteConfigFlags::NORMAL)
        .unwrap();

    assert_eq!(config.group_list().len(), 1);
    assert_eq!(config.has_group("General"), true);

    unsafe {
        config.delete_group("General", WriteConfigFlags::NORMAL);
    }
    assert_eq!(config.has_group("General"), false);

    // assert_eq!(config.group_list().len(), 0);

    assert_eq!(config.access_mode(), AccessMode::NoAccess);
}

#[test]
fn kconfiggroupclass() {
    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general = config.group("General");
    let mut group1 = general.group("Group1");
    let mut group2 = general.group("Group2");

    group1
        .write_qvariant_entry("someBool", true.into(), WriteConfigFlags::NORMAL)
        .unwrap();
    group2
        .write_qvariant_entry("someBool", true.into(), WriteConfigFlags::NORMAL)
        .unwrap();

    assert_eq!(config.group_list().len(), 1);
    assert_eq!(general.group_list().len(), 2);

    assert_eq!(config.has_group("General"), true);
    assert_eq!(general.has_group("Group1"), true);
    assert_eq!(config.has_group("Group1"), false);

    unsafe {
        general.delete_group("Group1", WriteConfigFlags::NORMAL);
    }
    assert_eq!(general.has_group("Group1"), false);

    // assert_eq!(config.group_list().len(), 0);

    assert_eq!(config.access_mode(), AccessMode::NoAccess);
}
