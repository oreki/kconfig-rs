#[test]
#[allow(unused)]
fn kconfig_class() {
    use kconfig::prelude::*;
    use qttypes::QStandardPathLocation;

    let config = KConfig::with_file("myapprc");

    let full_path = KConfig::with_file("/etc/kderc");

    let global_free = KConfig::new(
        "config",
        OpenFlags::NO_GLOBALS,
        QStandardPathLocation::AppDataLocation,
    );

    let simple_config = KConfig::new(
        "simple_rc",
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::AppDataLocation,
    );

    let data_resource = KConfig::new(
        "data",
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::AppDataLocation,
    );

    let custom_backend =
        KConfig::with_backend("config", "backend", QStandardPathLocation::AppDataLocation);
}

#[test]
#[allow(unused)]
fn special_configuration_object() {
    use kconfig::prelude::*;

    let config = KSharedConfigPtr::default();
    let config = KConfig::default();
}

#[test]
#[allow(unused)]
fn commonly_useful_methods() {
    use kconfig::prelude::*;

    env_logger::init();

    let config = KSharedConfigPtr::default();
    let group_list = config.group_list();

    for group in &group_list {
        log::debug!("next group: {}", group);
    }
}

#[test]
#[allow(unused)]
fn ksharedconfig() {
    use kconfig::prelude::*;
    use qttypes::QStandardPathLocation;

    let config = KSharedConfig::open_config(
        "ksomefilerc",
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
}

#[test]
#[allow(unused)]
fn kconfiggroup() {
    use kconfig::prelude::*;
    use qttypes::{QStandardPathLocation, QString};

    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general_group = config.group("General");
    let colors_group = config.group("Colors");
    let sub_group2 = general_group.group("Dialogs");
}

#[test]
#[allow(unused)]
fn read_entries() {
    use kconfig::prelude::*;
    use qmetaobject::QMetaType;
    use qttypes::{QColor, QStandardPathLocation, QString, QStringList};

    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general_group = config.group("General");
    let mut colors_group = config.group("Colors");

    let account_name = general_group.read_qstring_entry("Account");
    let color = QColor::from_qvariant(
        colors_group
            .read_qvariant_entry("background", QColor::from_name("white").to_qvariant())
            .unwrap_or(QColor::from_name("white").to_qvariant()),
    )
    .unwrap();

    assert!(QColor::from_name("white") == color);

    let list = QStringList::from_qvariant(
        general_group
            .read_qvariant_entry("List", QStringList::default().into())
            .unwrap_or(QStringList::default().into()),
    );
    let path = general_group.read_path_entry("SaveTo");
}

#[test]
#[allow(unused)]
fn writing_entries() {
    use kconfig::prelude::*;
    use qmetaobject::QMetaType;
    use qttypes::{QColor, QStandardPathLocation, QString};

    let mut config = KConfig::new(
        QString::default(),
        OpenFlags::SIMPLE_CONFIG,
        QStandardPathLocation::GenericConfigLocation,
    );
    let mut general_group = config.group("General");
    let mut color_group = config.group("Colors");

    general_group
        .write_string_entry("Account", "accountName", WriteConfigFlags::NORMAL)
        .unwrap();
    general_group.write_path_entry("SaveTo", "savePath".into(), WriteConfigFlags::NORMAL);
    color_group
        .write_qvariant_entry(
            "background",
            QColor::from_name("white").to_qvariant(),
            WriteConfigFlags::NORMAL,
        )
        .unwrap();
    config.sync();
}
