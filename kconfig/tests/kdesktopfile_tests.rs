use kconfig::kdesktopfile::KDesktopFile;
// use qttypes::QStandardPathLocation;

// #[test]
// fn basic() {
//     const FILE_NAME: &str = "org.kde.kasts.desktop";
//
//     let desktop_file = KDesktopFile::new(
//         QStandardPathLocation::ApplicationsLocation,
//         FILE_NAME.into(),
//     );
//
//     assert_eq!(desktop_file.file_name(), FILE_NAME.into());
//     assert_eq!(desktop_file.read_name(), Some("Kasts".into()));
//     assert_eq!(
//         desktop_file.read_generic_name(),
//         Some("Podcast Application".into())
//     );
//     assert_eq!(desktop_file.read_path(), None);
//     assert_eq!(desktop_file.read_type(), Some("Application".into()));
//     assert_eq!(desktop_file.read_url(), None);
//     assert_eq!(
//         desktop_file.read_comment(),
//         Some("Mobile Podcast Application".into())
//     );
//     assert_eq!(desktop_file.read_doc_path(), None);
//     assert_eq!(desktop_file.read_icon(), Some("kasts".into()));
//
//     assert!(desktop_file.has_application_type());
//     assert!(!desktop_file.has_link_type());
//     assert!(!desktop_file.has_device_type());
//     assert!(!desktop_file.no_display());
//     assert!(desktop_file.try_exec());
//
//     assert_eq!(desktop_file.read_actions().len(), 0);
//     assert_eq!(desktop_file.read_mime_types().len(), 0);
// }

#[test]
fn static_methods() {
    assert!(!KDesktopFile::is_desktop_file("abc".into()));
    assert!(KDesktopFile::is_desktop_file("abc.desktop".into()));
}

// #[test]
// fn action_group() {
//     const FILE_NAME: &str = "org.kde.kasts.desktop";
//
//     let desktop_file = KDesktopFile::new(
//         QStandardPathLocation::ApplicationsLocation,
//         FILE_NAME.into(),
//     );
//
//     const ACTION_GROUP: &str = "abc";
//
//     let mut action_group = desktop_file.action_group(ACTION_GROUP.into());
//     action_group.write_entry("abc".into(), true.into(), WriteConfigFlags::NORMAL);
//
//     assert!(desktop_file.has_action_group(ACTION_GROUP.into()));
// }
