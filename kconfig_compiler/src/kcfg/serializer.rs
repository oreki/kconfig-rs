use heck::{ToSnakeCase, ToUpperCamelCase};
use quote::quote;

use crate::kcfgc::{Kcfgc, MemberVaiables};

use super::representation;

struct GenTokenStreamReturn {
    struct_field: proc_macro2::TokenStream,
    impl_methods: proc_macro2::TokenStream,
    new_fn: proc_macro2::TokenStream,
}

pub fn generate_kcfg_tokenstream(
    kcfgc: &Kcfgc,
    kcfg: &representation::Kcfg,
) -> proc_macro2::TokenStream {
    let struct_name = syn::Ident::new(
        kcfgc.struct_name.to_upper_camel_case().as_str(),
        proc_macro2::Span::mixed_site(),
    );

    let GenTokenStreamReturn {
        struct_field,
        impl_methods,
        new_fn,
    } = kcfg.generate_tokenstream(kcfgc);

    let config_name = &kcfg.file_name;
    let object_return = kcfg.generate_new_object();

    let (singleton_fn, global) = generate_singleton_fn(&struct_name, kcfgc.singleton);
    let default_impl = generate_default_impl(&struct_name, &config_name, &new_fn, &object_return);

    quote! {
        #[derive(Debug)]
        pub struct #struct_name {
            _config: kconfig::kcoreconfigskeleton::KCoreConfigSkeleton,
            #struct_field
        }

        impl #struct_name {
            #singleton_fn

            #impl_methods
        }

        #default_impl

        #global
    }
}

impl representation::Kcfg {
    fn generate_tokenstream(&self, options: &Kcfgc) -> GenTokenStreamReturn {
        let (struct_fields, impl_methods, new_fn) = self.groups.iter().fold(
            (Vec::new(), Vec::new(), Vec::new()),
            |(mut struct_fields, mut impl_methods, mut new_fn), group| {
                let GenTokenStreamReturn {
                    struct_field: s,
                    impl_methods: i,
                    new_fn: n,
                } = group.generate_tokenstream(options);
                struct_fields.push(s);
                impl_methods.push(i);
                new_fn.push(n);
                (struct_fields, impl_methods, new_fn)
            },
        );

        GenTokenStreamReturn {
            struct_field: struct_fields.into_iter().collect(),
            impl_methods: impl_methods.into_iter().collect(),
            new_fn: new_fn.into_iter().collect(),
        }
    }

    fn generate_new_object(&self) -> proc_macro2::TokenStream {
        let entries = self.groups.iter().fold(Vec::new(), |mut acc, group| {
            acc.extend(group.get_entries_name());
            acc
        });

        let entries_getters: Vec<proc_macro2::TokenStream> = entries
            .iter()
            .map(|x| {
                let entry = syn::Ident::new(x, proc_macro2::Span::call_site());
                let default_getter_function = syn::Ident::new(
                    &format!("get_default_{}", x),
                    proc_macro2::Span::call_site(),
                );
                quote! {#entry: Self::#default_getter_function()}
            })
            .collect();

        quote! {
            Self {
                _config: config,
                #(#entries_getters,)*
            }
        }
    }
}

impl representation::Group {
    fn generate_tokenstream(&self, options: &Kcfgc) -> GenTokenStreamReturn {
        let (struct_fields, impl_methods, new_fn) = self.entries.iter().fold(
            (Vec::new(), Vec::new(), Vec::new()),
            |(mut struct_fields, mut impl_methods, mut new_fn), x| {
                let temp = x.generate_tokenstream(options);
                struct_fields.push(temp.struct_field);
                impl_methods.push(temp.impl_methods);
                new_fn.push(temp.new_fn);
                (struct_fields, impl_methods, new_fn)
            },
        );

        let group_name = &self.name;
        let new_fn: proc_macro2::TokenStream = new_fn.into_iter().collect();
        let new_fn = quote! {
            config.set_current_group(qttypes::QString::from(#group_name));
            #new_fn
        };

        GenTokenStreamReturn {
            struct_field: struct_fields.into_iter().collect(),
            impl_methods: impl_methods.into_iter().collect(),
            new_fn,
        }
    }

    fn get_entries_name(&self) -> Vec<String> {
        self.entries
            .iter()
            .map(|x| x.name.to_snake_case())
            .collect()
    }
}

impl representation::KcfgEntry {
    fn generate_tokenstream(&self, options: &Kcfgc) -> GenTokenStreamReturn {
        match &self.entry_type {
            representation::EntryType::Int(entry) => {
                entry.generate_tokenstream(&self.name, options)
            }
            representation::EntryType::UInt(entry) => {
                entry.generate_tokenstream(&self.name, options)
            }
            representation::EntryType::Int64(entry) => {
                entry.generate_tokenstream(&self.name, options)
            }
            representation::EntryType::UInt64(entry) => {
                entry.generate_tokenstream(&self.name, options)
            }
            representation::EntryType::Double(entry) => {
                entry.generate_tokenstream(&self.name, options)
            }
            representation::EntryType::Bool(entry) => {
                entry.generate_tokenstream(&self.name, options)
            }
            _ => todo!(),
        }
    }
}

impl representation::BasicType<i64> {
    fn generate_tokenstream(&self, name: &str, options: &Kcfgc) -> GenTokenStreamReturn {
        let field_type = syn::Type::Verbatim(quote! {i64});
        generate_basic_type(name, &field_type, &self.default_value, options)
    }
}

impl representation::BasicType<u64> {
    fn generate_tokenstream(&self, name: &str, options: &Kcfgc) -> GenTokenStreamReturn {
        let field_type = syn::Type::Verbatim(quote! {u64});
        generate_basic_type(name, &field_type, &self.default_value, options)
    }
}

impl representation::BasicType<f64> {
    fn generate_tokenstream(&self, name: &str, options: &Kcfgc) -> GenTokenStreamReturn {
        let field_type = syn::Type::Verbatim(quote! {f64});
        generate_basic_type(name, &field_type, &self.default_value, options)
    }
}

impl representation::BasicType<i32> {
    fn generate_tokenstream(&self, name: &str, options: &Kcfgc) -> GenTokenStreamReturn {
        let field_type = syn::Type::Verbatim(quote! {i32});
        generate_basic_type(name, &field_type, &self.default_value, options)
    }
}

impl representation::BasicType<u32> {
    fn generate_tokenstream(&self, name: &str, options: &Kcfgc) -> GenTokenStreamReturn {
        let field_type = syn::Type::Verbatim(quote! {u32});
        generate_basic_type(name, &field_type, &self.default_value, options)
    }
}

impl representation::BasicType<bool> {
    fn generate_tokenstream(&self, name: &str, options: &Kcfgc) -> GenTokenStreamReturn {
        let field_type = syn::Type::Verbatim(quote! {bool});
        generate_basic_type(name, &field_type, &self.default_value, options)
    }
}

fn generate_struct_field(
    option: &MemberVaiables,
    name: &syn::Ident,
    field_type: &syn::Type,
) -> proc_macro2::TokenStream {
    match option {
        MemberVaiables::Private => quote! { #name: #field_type, },
        MemberVaiables::Public => quote! { pub #name: #field_type, },
        MemberVaiables::Crate => quote! { pub(crate) #name: #field_type, },
    }
}

fn generate_getter_function(name: &syn::Ident, field_type: &syn::Type) -> proc_macro2::TokenStream {
    let getter_function = syn::Ident::new(&format!("get_{}", name), proc_macro2::Span::call_site());
    quote! {
        pub fn #getter_function(&self) -> #field_type {
            self.#name
        }
    }
}

fn generate_setter_function(name: &syn::Ident, field_type: &syn::Type) -> proc_macro2::TokenStream {
    let setter_function = syn::Ident::new(&format!("set_{}", name), proc_macro2::Span::call_site());
    quote! {
        pub fn #setter_function(&mut self, value: #field_type) {
            self.#name = value;
        }
    }
}

fn generate_default_getter_function<T>(
    name: &syn::Ident,
    field_type: &syn::Type,
    default_value: &Option<T>,
    public: bool,
) -> proc_macro2::TokenStream
where
    T: Default + quote::ToTokens,
{
    let def = match default_value {
        Some(x) => quote! {#x},
        None => quote! {#field_type::default()},
    };
    let default_getter_function = syn::Ident::new(
        &format!("get_default_{}", name),
        proc_macro2::Span::call_site(),
    );
    if public {
        quote! {
            pub fn #default_getter_function() -> #field_type {
                #def
            }
        }
    } else {
        quote! {
            fn #default_getter_function() -> #field_type {
                #def
            }
        }
    }
}

fn generate_new_fn_entry(name: &str) -> proc_macro2::TokenStream {
    let default_getter_function = syn::Ident::new(
        &format!("get_default_{}", name.to_snake_case()),
        proc_macro2::Span::call_site(),
    );

    quote! {
        config.add_item(#name, Self::#default_getter_function());
    }
}

fn generate_singleton_fn(
    struct_name: &syn::Ident,
    singleton: bool,
) -> (proc_macro2::TokenStream, proc_macro2::TokenStream) {
    if singleton {
        let global = quote! {
            thread_local! {
                static CURRENT_CONFIG: std::sync::RwLock<std::sync::Arc<#struct_name>> = std::sync::RwLock::new(std::sync::Arc::new(#struct_name::default()));
            }
        };

        let function = quote! {
            pub fn singleton() -> std::sync::Arc<Self> {
                CURRENT_CONFIG.with(|c| c.read().unwrap().clone())
            }

            pub fn swap_singleton(self) {
                CURRENT_CONFIG.with(|c| *c.write().unwrap() = std::sync::Arc::new(self))
            }
        };
        (function, global)
    } else {
        let t = quote! {};
        (t.clone(), t)
    }
}

fn generate_default_impl(
    struct_name: &syn::Ident,
    config_name: &str,
    new_fn: &proc_macro2::TokenStream,
    object_return: &proc_macro2::TokenStream,
) -> proc_macro2::TokenStream {
    quote! {
        impl Default for #struct_name {
            fn default() -> Self {
                let mut config = kconfig::kcoreconfigskeleton::KCoreConfigSkeleton::new(#config_name);
                #new_fn
                #object_return
            }
        }
    }
}

trait BasicType {
    fn generate_default_getter(&self, name_ident: &syn::Ident);
}

fn generate_basic_type<T>(
    name: &str,
    field_type: &syn::Type,
    default_value: &Option<T>,
    options: &Kcfgc,
) -> GenTokenStreamReturn
where
    T: Default + quote::ToTokens,
{
    let name_ident = syn::Ident::new(&name.to_snake_case(), proc_macro2::Span::call_site());
    let mut impl_methods = Vec::new();

    impl_methods.push(generate_getter_function(&name_ident, &field_type));

    if options.mutators.generate_for_key(name) {
        impl_methods.push(generate_setter_function(&name_ident, &field_type));
    }

    impl_methods.push(generate_default_getter_function(
        &name_ident,
        &field_type,
        default_value,
        options.default_value_getters.generate_for_key(name),
    ));

    let new_fn = generate_new_fn_entry(name);

    GenTokenStreamReturn {
        struct_field: generate_struct_field(&options.member_variables, &name_ident, &field_type),
        impl_methods: impl_methods.into_iter().collect(),
        new_fn,
    }
}
