#[derive(Debug, PartialEq, Clone)]
pub struct Kcfg {
    pub(crate) file_name: String,
    pub(crate) groups: Vec<Group>,
}

impl Kcfg {
    pub fn new(file_name: String, groups: Vec<Group>) -> Self {
        Self { file_name, groups }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Group {
    pub(crate) name: String,
    pub(crate) entries: Vec<KcfgEntry>,
}

impl Group {
    pub fn new(name: String, entries: Vec<KcfgEntry>) -> Self {
        Self { name, entries }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct KcfgEntry {
    pub(crate) name: String,
    pub(crate) entry_type: EntryType,
}

impl KcfgEntry {
    pub fn new(name: String, entry_type: EntryType) -> Self {
        Self { name, entry_type }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum EntryType {
    Int(BasicType<i32>),
    UInt(BasicType<u32>),
    Int64(BasicType<i64>),
    UInt64(BasicType<u64>),
    Double(BasicType<f64>),
    Bool(BasicType<bool>),
    Enum(EnumEntry),
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct EnumEntry {
    pub(crate) choices: EnumChoices,
    pub(crate) label: Option<String>,
}

impl EnumEntry {
    pub fn new(choices: EnumChoices, label: Option<String>) -> Self {
        Self { choices, label }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Default)]
pub struct EnumChoices {
    pub(crate) default_value: Option<String>,
    pub(crate) choices: Vec<Choice>,
}

impl EnumChoices {
    pub fn new(default_value: Option<String>, choices: Vec<Choice>) -> Self {
        Self {
            default_value,
            choices,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Choice {
    pub(crate) name: String,
    pub(crate) label: Option<String>,
}

impl Choice {
    pub fn new(name: String, label: Option<String>) -> Self {
        Self { name, label }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct BasicType<T> {
    pub(crate) label: Option<String>,
    pub(crate) default_value: Option<T>,
}

impl<T> BasicType<T> {
    pub fn new(label: Option<String>, default_value: Option<T>) -> Self {
        Self {
            label,
            default_value,
        }
    }
}
