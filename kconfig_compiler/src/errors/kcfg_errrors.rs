use std::fmt::{Debug, Display};

#[derive(Debug, PartialEq)]
pub enum SerializeError {
    NameAttributeAbsent(String),
    ParseError,
    EntryTypeError(EntryTypeError),
}

impl Display for SerializeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NameAttributeAbsent(x) => write!(f, "Name attribute required in tag: {}", x),
            Self::ParseError => write!(f, "Error in parsing given kcfg"),
            Self::EntryTypeError(err) => err.fmt(f),
        }
    }
}

impl From<EntryTypeError> for SerializeError {
    fn from(err: EntryTypeError) -> Self {
        Self::EntryTypeError(err)
    }
}

#[derive(Debug, PartialEq)]
pub enum EntryTypeError {
    UnsupportedType,
    TypeAttributeAbsent,
    EnumEntryError(EnumEntryError),
    ParseError(String),
}

impl From<EnumEntryError> for EntryTypeError {
    fn from(err: EnumEntryError) -> Self {
        Self::EnumEntryError(err)
    }
}

impl From<std::num::ParseIntError> for EntryTypeError {
    fn from(_: std::num::ParseIntError) -> Self {
        Self::ParseError("Error in Parsing Int Entry".to_string())
    }
}

impl From<std::str::ParseBoolError> for EntryTypeError {
    fn from(_: std::str::ParseBoolError) -> Self {
        Self::ParseError("Error in Parsing bool".to_string())
    }
}

impl From<std::num::ParseFloatError> for EntryTypeError {
    fn from(_: std::num::ParseFloatError) -> Self {
        Self::ParseError("Error in Parsing Float".to_string())
    }
}

#[derive(Debug, PartialEq)]
pub enum EnumEntryError {}

impl Display for EnumEntryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "")
    }
}

#[derive(Debug, PartialEq)]
pub enum DeserializeError {
    AddingFieldFailed(String),
}

impl Display for DeserializeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::AddingFieldFailed(x) => write!(f, "Error in adding file: {}", x),
        }
    }
}
