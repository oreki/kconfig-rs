use std::str::FromStr;

pub mod errors;
pub mod kcfg;
pub mod kcfgc;

pub fn kcfg_to_tokenstream(
    kcfgc: &kcfgc::Kcfgc,
    xml: &str,
) -> Result<proc_macro2::TokenStream, errors::Error> {
    let rust_representation = kcfg::Kcfg::from_str(xml)?;
    Ok(kcfg::generate_kcfg_tokenstream(
        &kcfgc,
        &rust_representation,
    ))
}
