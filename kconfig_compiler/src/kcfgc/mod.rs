use crate::errors::KcfgcDeserializeError;
use std::{collections::HashSet, path::PathBuf, str::FromStr};

mod deserialize;

#[derive(Debug, PartialEq, Clone)]
pub struct Kcfgc {
    pub(crate) file: PathBuf,
    pub(crate) module: Option<String>,
    pub(crate) struct_name: String,
    pub(crate) singleton: bool,
    pub(crate) member_variables: MemberVaiables,
    pub(crate) mutators: BoolOrList,
    pub(crate) notifiers: BoolOrList,
    pub(crate) default_value_getters: BoolOrList,
    pub(crate) generate_properties: bool,
    pub(crate) parent_in_constructor: bool,
    pub(crate) translation_system: TranslationSystem,
}

impl Kcfgc {
    pub fn get_kcfg_contents(&self) -> Result<String, std::io::Error> {
        let p = std::env::current_dir()?.join(self.file.as_path());
        std::fs::read_to_string(p)
    }

    pub fn from_file(file: &str) -> Result<Self, KcfgcDeserializeError> {
        let p = std::env::current_dir().unwrap().join(file);
        let x = std::fs::read_to_string(p).unwrap();
        Self::from_str(&x)
    }
}

impl Default for Kcfgc {
    fn default() -> Self {
        const ERROR_MSG: &str = "Error in Reading Executable Name";
        let program_path = std::env::current_exe().expect(ERROR_MSG);
        Self {
            file: program_path.with_extension(".kcfg"),
            module: None,
            struct_name: program_path
                .file_name()
                .expect(ERROR_MSG)
                .to_string_lossy()
                .to_string(),
            singleton: false,
            member_variables: MemberVaiables::default(),
            mutators: BoolOrList::default(),
            notifiers: BoolOrList::default(),
            default_value_getters: BoolOrList::default(),
            generate_properties: false,
            parent_in_constructor: false,
            translation_system: TranslationSystem::default(),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum MemberVaiables {
    Public,
    Private,
    Crate,
}

impl Default for MemberVaiables {
    fn default() -> Self {
        Self::Private
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum BoolOrList {
    Boolean(bool),
    List(HashSet<String>),
}

impl BoolOrList {
    pub fn generate_for_key(&self, key: &str) -> bool {
        match self {
            BoolOrList::Boolean(x) => *x,
            BoolOrList::List(x) => x.contains(key),
        }
    }
}

impl Default for BoolOrList {
    fn default() -> Self {
        Self::Boolean(false)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum TranslationSystem {
    Qt,
    KDE,
}

impl Default for TranslationSystem {
    fn default() -> Self {
        Self::Qt
    }
}
