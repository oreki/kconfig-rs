use std::{collections::HashMap, str::FromStr};

use crate::errors::KcfgcDeserializeError;

use super::{BoolOrList, Kcfgc, MemberVaiables, TranslationSystem};

impl FromStr for Kcfgc {
    type Err = KcfgcDeserializeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        const FILE_KEY: &str = "File";
        const MODULE_KEY: &str = "Module";
        const STRUCT_NAME_KEY: &str = "StructName";
        const SINGLETON_KEY: &str = "Singleton";
        const GENERATE_PROPERTIES_KEY: &str = "GenerateProperties";
        const PARENT_IN_CONSTRUCTOR_KEY: &str = "ParentInConstructor";
        const MEMBER_VARIABLES_KEY: &str = "MemberVaiables";
        const TRANSLATION_SYSTEM_KEY: &str = "TranslationSystem";
        const MUTATORS_KEY: &str = "Mutators";
        const NOTIFIERS_KEY: &str = "Notifiers";
        const DEFAULT_VALUE_GETTERS_KEY: &str = "DefaultValueGetters";

        let keyval = s
            .trim()
            .lines()
            .map(|x| x.trim().split("=").collect::<Vec<&str>>())
            .map(|x| {
                if x.len() != 2 {
                    Err(Self::Err::InvalidKcfgc)
                } else {
                    Ok((x[0], x[1]))
                }
            })
            .collect::<Result<HashMap<&str, &str>, Self::Err>>()?;

        let file = keyval
            .get(FILE_KEY)
            .ok_or(Self::Err::ExpectedString(FILE_KEY.to_string()))?
            .into();
        let module = keyval.get(MODULE_KEY).map(|x| x.to_string());
        let struct_name = keyval
            .get(STRUCT_NAME_KEY)
            .ok_or(Self::Err::ExpectedString(STRUCT_NAME_KEY.to_string()))?
            .to_string();
        let singleton = match keyval.get(SINGLETON_KEY) {
            Some(x) => bool::from_str(x)
                .map_err(|_| Self::Err::ExpectedBoolean(SINGLETON_KEY.to_string()))?,
            None => false,
        };
        let generate_properties = match keyval.get(GENERATE_PROPERTIES_KEY) {
            Some(x) => bool::from_str(x)
                .map_err(|_| Self::Err::ExpectedBoolean(GENERATE_PROPERTIES_KEY.to_string()))?,
            None => false,
        };
        let parent_in_constructor = match keyval.get(PARENT_IN_CONSTRUCTOR_KEY) {
            Some(x) => bool::from_str(x)
                .map_err(|_| Self::Err::ExpectedBoolean(PARENT_IN_CONSTRUCTOR_KEY.to_string()))?,
            None => false,
        };
        let member_variables = match keyval.get(MEMBER_VARIABLES_KEY) {
            Some(x) => MemberVaiables::from_str(x)?,
            None => MemberVaiables::Private,
        };
        let translation_system = match keyval.get(TRANSLATION_SYSTEM_KEY) {
            Some(x) => TranslationSystem::from_str(x)?,
            None => TranslationSystem::Qt,
        };
        let mutators = match keyval.get(MUTATORS_KEY) {
            Some(x) => BoolOrList::from_str(x)?,
            None => BoolOrList::Boolean(false),
        };
        let notifiers = match keyval.get(NOTIFIERS_KEY) {
            Some(x) => BoolOrList::from_str(x)?,
            None => BoolOrList::Boolean(false),
        };
        let default_value_getters = match keyval.get(DEFAULT_VALUE_GETTERS_KEY) {
            Some(x) => BoolOrList::from_str(x)?,
            None => BoolOrList::Boolean(false),
        };

        Ok(Self {
            file,
            module,
            struct_name,
            singleton,
            generate_properties,
            parent_in_constructor,
            member_variables,
            translation_system,
            mutators,
            notifiers,
            default_value_getters,
        })
    }
}

impl FromStr for TranslationSystem {
    type Err = KcfgcDeserializeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "qt" => Ok(Self::Qt),
            "kde" => Ok(Self::KDE),
            _ => Err(Self::Err::InvalidTranslationSystem(s.to_string())),
        }
    }
}

impl FromStr for MemberVaiables {
    type Err = KcfgcDeserializeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "public" => Ok(Self::Public),
            "private" => Ok(Self::Private),
            "crate" => Ok(Self::Crate),
            _ => Err(Self::Err::InvalidMemberVariables(s.to_string())),
        }
    }
}

impl FromStr for BoolOrList {
    type Err = KcfgcDeserializeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "true" => Ok(Self::Boolean(true)),
            "false" => Ok(Self::Boolean(false)),
            _ => Ok(Self::List(s.split(",").map(|x| x.to_string()).collect())),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use super::*;

    #[test]
    fn all_values() {
        let kcfgc = r#"
            StructName=Test12
            File=test12.kcfg
            Module=test12
            Singleton=true
            GenerateProperties=true
            ParentInConstructor=true
            MemberVaiables=public
            TranslationSystem=kde
            Mutators=abc,efg
            Notifiers=abc,efg
            DefaultValueGetters=abc,efg"#;

        let ans = Kcfgc {
            file: "test12.kcfg".into(),
            module: Some("test12".to_string()),
            struct_name: "Test12".to_string(),
            singleton: true,
            member_variables: MemberVaiables::Public,
            generate_properties: true,
            parent_in_constructor: true,
            translation_system: TranslationSystem::KDE,
            mutators: BoolOrList::List(HashSet::from(["abc".to_string(), "efg".to_string()])),
            notifiers: BoolOrList::List(HashSet::from(["abc".to_string(), "efg".to_string()])),
            default_value_getters: BoolOrList::List(HashSet::from([
                "abc".to_string(),
                "efg".to_string(),
            ])),
        };

        let kcfgc = Kcfgc::from_str(kcfgc).unwrap();
        assert_eq!(ans, kcfgc);
    }

    #[test]
    fn default_values() {
        let kcfgc = r#"
            StructName=Test12
            File=test12.kcfg"#;

        let ans = Kcfgc {
            file: "test12.kcfg".into(),
            module: None,
            struct_name: "Test12".to_string(),
            singleton: false,
            member_variables: MemberVaiables::Private,
            generate_properties: false,
            parent_in_constructor: false,
            translation_system: TranslationSystem::Qt,
            mutators: BoolOrList::Boolean(false),
            notifiers: BoolOrList::Boolean(false),
            default_value_getters: BoolOrList::Boolean(false),
        };

        assert_eq!(ans, Kcfgc::from_str(kcfgc).unwrap());
    }
}
